#include <iostream>
#include <memory>
#include "AST/AST.h"
#include "grammar/grammar.h"

int main(int argc, char** argv)
{
    Ordinis::AST::ModuleNode* mod(0);
    if (argc > 1)
        mod = Ordinis::grammar::parse_file(argv[1]);
    else
        mod = Ordinis::grammar::parse(std::cin);
    std::cout << mod << std::endl;
    return 0;
}