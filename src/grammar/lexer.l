%{
    #include <string>
    #include <cstdlib>
    #include <iostream>
    
    #include "grammar/lexer.h"
    
    #define yyterminate() return Parser::make_END(loc)
    #define YY_USER_ACTION loc.columns(yyleng);
%}

%option noyywrap nounput nodefault stack

/* C++ */
%option c++ yyclass="Lexer" prefix="ord"

%%
%{
    loc.step();
%}

\+                          return Parser::make_PLUS(yytext, loc);
\-                          return Parser::make_MINUS(yytext, loc);
\*                          return Parser::make_STAR(yytext, loc);
\/                          return Parser::make_SLASH(yytext, loc);
\^                          return Parser::make_POW(yytext, loc);
[0-9]+                      return Parser::make_DOUBLE(strtod(yytext, 0), loc);     /* TODO: construct double regex */
[a-zA-Z_][a-zA-Z0-9_]*      return Parser::make_IDENT(std::string(yytext), loc);
\/\/.*                      //ignore: line comment
.                           loc.step();
\n                          loc.lines(), loc.step();
<<EOF>>                     return Parser::make_eof(loc);

%%
#ifdef yylex
#undef yylex
#endif