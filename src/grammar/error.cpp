#include "grammar/error.h"

namespace Ordinis
{
namespace grammar
{
    ParseError::ParseError(const char* msg) : std::exception()
    {
        this->msg = msg;
    }
    
    const char* ParseError::what() const throw()
    {
        return msg;
    }
} //namespace grammar
} //namespace Ordinis