%require "3.0"

//C++
%skeleton "lalr1.cc"
%defines "parser.h"
%define api.namespace { Ordinis::grammar }
%define parser_class_name { Parser }

%define api.token.constructor
%define api.value.type variant
%define parse.assert
%define parse.error verbose

//Outside interaction
%parse-param { Ordinis::grammar::Lexer &lexer }
%parse-param { Ordinis::AST::ModuleNode* module }
%locations

%code requires {
    #include <iostream>
    #include <string>
    #include "AST/node.h"
    
    //Avoid recursive include
    namespace Ordinis{ namespace grammar{ class Lexer; }}
}


%code {
    //Avoid recursive include
    #include "grammar/lexer.h"
    
    #undef yylex
    #define yylex lexer.lex
}

%token <std::string>
    PLUS "+"
    MINUS "-"
    STAR "*"
    SLASH "/"
    POW "^"
;
%token <double> DOUBLE "double litteral"
%token <std::string> IDENT "identifier"
%token eof 0 "end of file"

%type <AST::IdentifierNode*> ident
%type <AST::DoubleNode*> double
%type <AST::ExpressionNode*> expr
%type <AST::BinopNode*> binop

%left PLUS MINUS
%left STAR SLASH
%right POW

%%
start :
        expr start
      | 
        eof
      ;
      
expr :
        ident   {
                $$ = $1;
                }
      |
        double  {
                $$ = $1;
                }
      | 
        binop   {
                $$ = $1;
                }
      ;
    
binop :
        expr PLUS expr  {
                        $$ = new AST::BinopNode($1, $2, $3);
                        }
      |
        expr MINUS expr {
                        $$ = new AST::BinopNode($1, $2, $3);
                        }
      |
        expr STAR expr {
                        $$ = new AST::BinopNode($1, $2, $3);
                        }
      |
        expr SLASH expr {
                        $$ = new AST::BinopNode($1, $2, $3);
                        }
      |
        expr POW expr {
                      $$ = new AST::BinopNode($1, $2, $3);
                      }
      ;
      
ident :
        IDENT {
                $$ = new AST::IdentifierNode($1);
                }
                
      ;

double :
        DOUBLE {
                $$ = new AST::DoubleNode($1);
                }
%%

namespace Ordinis
{
namespace grammar
{
    void Parser::error(const location_type& l, const std::string& m)
    {
        std::cerr << "error: " << l << ": " << m << std::endl;
    }
}
}

#undef yylex