#include <fstream>
#include <sstream>

#include "parser.h"
#include "grammar/grammar.h"
#include "grammar/lexer.h"
#include "grammar/error.h"
#include "AST/node.h"

namespace Ordinis
{
namespace grammar
{
    AST::ModuleNode* parse(std::istream& in)
    {
        AST::ModuleNode* mod = new AST::ModuleNode("module");
        Lexer l;
        l.switch_streams(&in);
        Parser p(l, mod);
        int res = p.parse();
        if (res != 0)
        {
            throw ParseError();
        }
        return mod;
    }
    
    AST::ModuleNode* parse_file(const std::string filename)
    {
        std::ifstream in(filename);
        return parse(in);
    }
    
    AST::ModuleNode* parse_string(const std::string input)
    {
        std::istringstream in(input);
        return parse(in);
    }
    
} //namespace grammar
} //namespace Ordinis