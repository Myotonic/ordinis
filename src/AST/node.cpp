#include "AST/node.h"
#include "AST/visitor.h"

namespace Ordinis
{
namespace AST
{
    //class Node
    Node::Node()
    {
    }
    
    Node::~Node()
    {
    }
    
    void Node::accept(Visitor* visitor)
    {
        visitor->visit(this);
    }
    
    //class ModuleNode
    ModuleNode::ModuleNode(std::string name) : Node()
    {
        this->name = name;
    }
    
    ModuleNode::~ModuleNode()
    {
    }
    
    std::string ModuleNode::getName()
    {
        return name;
    }
    
    //class ExpressionNode
    ExpressionNode::ExpressionNode() : Node()
    {
    }
    
    ExpressionNode::~ExpressionNode()
    {
    }
 
    //class IdentifierNode
    IdentifierNode::IdentifierNode(std::string name) : ExpressionNode()
    {
        this->name = name;
    }
    
    IdentifierNode::~IdentifierNode()
    {
    }
    
    std::string IdentifierNode::getName()
    {
        return name;
    }
    
    //class BinopNode
    BinopNode::BinopNode(ExpressionNode* lhs, std::string op, ExpressionNode* rhs) : ExpressionNode(), lhs(0), rhs(0)
    {
        this->lhs = lhs;
        this->op = op;
        this->rhs = rhs;
    }
    
    BinopNode::~BinopNode()
    {
        delete this->lhs;
        delete this->rhs;
    }
    
    ExpressionNode* BinopNode::getLhs()
    {
        return lhs;
    }
    
    ExpressionNode* BinopNode::getRhs()
    {
        return rhs;
    }
    
    std::string BinopNode::getOperator()
    {
        return op;
    }
    
    //class AssignmentNode
    AssignmentNode::AssignmentNode(IdentifierNode* var, ExpressionNode* value) : ExpressionNode(), value(0)
    {
        this->var = var;
        this->value = value;
    }
    
    AssignmentNode::~AssignmentNode()
    {
        delete this->value;
    }
    
    IdentifierNode* AssignmentNode::getVariable()
    {
        return var;
    }
    
    ExpressionNode* AssignmentNode::getValue()
    {
        return value;
    }
    
    //class DoubleNode
    DoubleNode::DoubleNode(double value) : ExpressionNode()
    {
        this->value = value;
    }
    
    DoubleNode::~DoubleNode()
    {
    }
    
    double DoubleNode::getValue()
    {
        return value;
    }
    
} //namespace AST
} //namespace Ordinis
