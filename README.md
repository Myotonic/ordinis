#Ordinis
##Continuous integration status
[![Build Status](https://drone.io/bitbucket.org/Myotonic/ordinis/status.png)](https://drone.io/bitbucket.org/Myotonic/ordinis/latest)

##Download
The latest builds can be found [Here](https://drone.io/bitbucket.org/Myotonic/ordinis/files "Latest builds")