#pragma once

#include <exception>

namespace Ordinis
{
namespace grammar
{
    class ParseError : public std::exception
    {
        public:
            ParseError(const char* msg="Parser error");
            virtual const char* what() const throw();
            
        private:
            const char* msg;
    };
} //namespace grammar
} //namespace Ordinis