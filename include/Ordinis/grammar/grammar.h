#pragma once

#include <string>
#include <istream>

#include "AST/node.h"

namespace Ordinis
{
namespace grammar
{
    Ordinis::AST::ModuleNode* parse(std::istream& in);
    Ordinis::AST::ModuleNode* parse_file(std::string filename);
    Ordinis::AST::ModuleNode* parse_string(std::string input);
} //namespace grammar
} //namespace Ordinis