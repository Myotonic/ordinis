#pragma once


#ifndef __FLEX_LEXER_H
#define yyFlexLexer ordFlexLexer
#include "FlexLexer.h"
#undef yyFlexLexer
#endif

#undef YY_DECL
#define YY_DECL Ordinis::grammar::Parser::symbol_type Ordinis::grammar::Lexer::lex()

#include "parser.h"

namespace Ordinis
{
namespace grammar
{
    
    class Grammar;
    
	class Lexer : public ordFlexLexer
	{
		public:
		    Lexer() {}
		    virtual ~Lexer() {}
		    
		    Parser::symbol_type lex();
		    Parser::location_type loc;
	};

} //namespace grammar
} //namespace Ordinsi