#pragma once
#include <string>

namespace Ordinis
{
namespace AST
{
    class Visitor;
    
    class Node
    {
        public:
            Node();
            virtual ~Node();
            
            virtual void accept(Visitor* visitor);
    };
    
    class ModuleNode : public Node
    {
        private:
            std::string name;
            
        public:
            ModuleNode(std::string name);
            virtual ~ModuleNode();
            
            std::string getName();
    };
    
    class ExpressionNode : public Node
    {
        public:
            ExpressionNode();
            virtual ~ExpressionNode();
    };
    
    class IdentifierNode : public ExpressionNode
    {
        private:
            std::string name;
            
        public:
            IdentifierNode(std::string name);
            virtual ~IdentifierNode();
            
            std::string getName();
    };
    
    class BinopNode : public ExpressionNode
    {
        private:
            ExpressionNode *lhs, *rhs;
            std::string op;
            
        public:
            BinopNode(ExpressionNode*, std::string, ExpressionNode*);
            virtual ~BinopNode();
            
            ExpressionNode* getLhs();
            ExpressionNode* getRhs();
            std::string getOperator();
    };
    
    class AssignmentNode : public ExpressionNode
    {
        private:
            IdentifierNode* var;
            ExpressionNode* value;
            
        public:
            AssignmentNode(IdentifierNode*, ExpressionNode*);
            virtual ~AssignmentNode();
            
            IdentifierNode* getVariable();
            ExpressionNode* getValue();
    };
    
    class DoubleNode : public ExpressionNode
    {
        private:
            double value;
            
        public:
            DoubleNode(double value);
            virtual ~DoubleNode();
            
            double getValue();
    };
    
} //namespace AST
} //namespace Ordinis