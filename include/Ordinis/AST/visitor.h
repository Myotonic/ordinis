#pragma once
#include "node.h"

namespace Ordinis
{
namespace AST
{
    class Visitor
    {
        public:
            virtual void visit(Node* node) = 0;
            virtual void visit(ModuleNode* mod) = 0;
            virtual void visit(IdentifierNode* ident) = 0;
            virtual void visit(BinopNode* binop) = 0;
            virtual void visit(AssignmentNode* assignment) = 0;
            virtual void visit(DoubleNode* dbl) = 0;
    };
    
} //namespace AST
} //namespace Ordinis